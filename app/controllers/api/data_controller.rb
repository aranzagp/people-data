# frozen_string_literal: true

module Api
  class DataController < ApiController
    def frequency_count
      frequency_count = PeopleService.retrieve_frequency_count
      render json: frequency_count
    rescue HttpRequestError => e
      render json: {
        status: "error",
        message: e.message,
        code: e.code
      }
    end

    def possible_duplicates
      retrieve_duplicates = PeopleService.retrieve_duplicates
      render json: retrieve_duplicates
    rescue HttpRequestError => e
      render json: {
        status: "error",
        message: e.message,
        code: e.code
      }
    end
  end
end
