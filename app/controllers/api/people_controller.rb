# frozen_string_literal: true

module Api
  class PeopleController < ApiController
    def index
      people_information = PeopleService.retrieve_people_information
      render json: people_information
    rescue HttpRequestError => e
      render json: {
        status: "error",
        message: e.message,
        code: e.code
      }
    end
  end
end
