import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from '../components/Home'
import Frequency from '../components/Frequency'
import Duplicates from '../components/Duplicates'

export default (
  <Router>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/frequency_count" exact component={Frequency} />
      <Route path="/possible_duplicates" exact component={Duplicates} />
    </Switch>
  </Router>
)
