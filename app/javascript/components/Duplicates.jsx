import React, { Component } from 'react'

class Duplicates extends Component {
  constructor (props) {
    super(props)
    this.state = { duplicates: [] }
  }

  componentDidMount () {
    const url = '/api/possible_duplicates'
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        }
        throw new Error('Network response was not ok.')
      })
      .then(response =>
        this.setState({ duplicates: response })
      )
      .catch(() => console.log('Error'))
  }

  renderDuplicatesComponent () {
    const { duplicates } = this.state
    if (duplicates.status === 'error') {
      return <h2>No Results</h2>
    }

    if (duplicates.length > 0) {
      const allDuplicates = duplicates.map((key, index) => (
        <li className="list-group-item" key={index}>
          <span>{key[0]} </span> <span>{key[1]} </span>
        </li>
      ))

      return (
        <ul className="list-group">
          {allDuplicates}
        </ul>
      )
    }
  }

  render () {
    return (
      <div className="d-flex align-items-center justify-content-center">
        <div className="jumbotron jumbotron-fluid bg-transparent">
          <div className="container secondary-color">
            <h1 className="display-4">Possible duplicates</h1>
            { this.renderDuplicatesComponent() }
          </div>
        </div>
      </div>
    )
  }
}

export default Duplicates
