import React, { Component } from 'react'

class Frecuency extends Component {
  constructor (props) {
    super(props)
    this.state = { people: [] }
  }

  componentDidMount () {
    const url = '/api/frequency_count'
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        }
        throw new Error('Network response was not ok.')
      })
      .then(response =>
        this.setState({ people: response })
      )
      .catch(() => console.log('Error'))
  }

  renderPeopleComponent () {
    const { people } = this.state

    if (people.status === 'error') {
      return <h2>No Results</h2>
    }

    if (people[0]) {
      const obj = people[0]

      const allPeople = Object.keys(obj).map((key, index) => (
        <li className="list-group-item" key={index}>
          <span>{key} </span> <span>{JSON.stringify(obj[key])} </span>
        </li>
      ))

      return (
        <ul className="list-group">
          {allPeople}
        </ul>
      )
    }
  }

  render () {
    return (
      <div className="d-flex align-items-center justify-content-center">
        <div className="jumbotron jumbotron-fluid bg-transparent">
          <div className="container secondary-color">
            <h1 className="display-4">Characters Frequency Count</h1>
            { this.renderPeopleComponent() }
          </div>
        </div>
      </div>
    )
  }
}

export default Frecuency
