import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Home extends Component {
  constructor (props) {
    super(props)
    this.state = { people: [] }
  }

  componentDidMount () {
    const url = '/api/people'
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        }
        throw new Error('Network response was not ok.')
      })
      .then(response =>
        this.setState({ people: response })
      )
      .catch(() => console.log('Error'))
  }

  renderPeopleComponent () {
    const { people } = this.state
    if (people.status === 'error') {
      return <h2>No Results</h2>
    }
    const allPeople = people.map((person, index) => (
      <tr key={index}>
        <td>{person.display_name}</td>
        <td>{person.email_address}</td>
        <td>{person.title}</td>
      </tr>
    ))
    return (
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Job Title</th>
          </tr>
        </thead>
        <tbody>
          {allPeople}
        </tbody>
      </table>
    )
  }

  render () {
    return (
      <div className="d-flex align-items-center justify-content-center">
        <div className="jumbotron jumbotron-fluid bg-transparent">
          <div className="container">
            <div className="d-flex justify-content-between">
              <Link
                to="/frequency_count"
                className="btn btn-primary"
                role="button"
              >
                Characters frequency count
              </Link>
              <Link
                to="/possible_duplicates"
                className="btn btn-primary"
                role="button"
              >
                Possible Duplicates
              </Link>
            </div>

            <h1 className="display-4">People List</h1>
            { this.renderPeopleComponent() }
          </div>
        </div>
      </div>
    )
  }
}

export default Home
