# frozen_string_literal: true

class PeopleService
  extend DataUtils

  PEOPLE_URL = 'https://api.salesloft.com/v2/people.json'
  HEADERS = { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" }.freeze

  def self.retrieve_people_information
    response = RequestService.get(PEOPLE_URL, HEADERS)
    response["data"].map { |item| item.slice("display_name", "email_address", "title") }
  end

  def self.retrieve_frequency_count
    response = RequestService.get(PEOPLE_URL, HEADERS)
    hash = {}
    response["data"].each do |person|
      email_hash = person.slice("email_address")
      email = email_hash["email_address"]
      hash[email] = frequency_count_by_email(email)
    end
    [hash]
  end

  def self.retrieve_duplicates
    response = RequestService.get(PEOPLE_URL, HEADERS)
    arr = []
    response["data"].each do |item|
      arr << item["email_address"]
    end
    retrieve_comparison_results(arr, [])
  end
end
