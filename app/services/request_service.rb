# frozen_string_literal: true

class RequestService
  def self.get(url, headers)
    make_request(:get, url, headers)
  end

  def self.make_request(verb, url, headers)
    response = HTTParty.send(verb, url, { headers: headers })

    if response.code != 200
      Rails.logger.error("Error making #{verb} request to #{url}: #{response.code}, #{response['error']}")
      raise HttpRequestError.new(response.code, response["error"], response, url)
    end
    response
  end
end
