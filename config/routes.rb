Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    resources :people, only: [:index]
    get 'frequency_count' => "data#frequency_count"
    get 'possible_duplicates' => "data#possible_duplicates"
  end

  root 'home#index'
  get '/*path' => 'home#index'
end
