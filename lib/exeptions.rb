# frozen_string_literal: true

class HttpRequestError < StandardError
  attr_accessor :code, :message, :response, :url

  def initialize(code, message, response, url)
    @code = code
    @response = response
    @url = url
    @message = "#{code} #{message} - #{url} - #{response}"

    super(message)
  end
end
