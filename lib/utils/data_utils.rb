# frozen_string_literal: true

module DataUtils
  def frequency_count_by_email(email)
    hash = {}
    array = email.gsub(/[^0-9a-z ]/i, '').split(//)
    array.each do |e|
      if hash.key?(e)
        hash[e] += 1
      else
        hash[e] = 1
      end
    end
    hash.sort_by { |_key, value| value }.reverse.to_h
  end

  def retrieve_comparison_results(array, duplicates)
    duplicates ||= []
    return duplicates if array.length.zero?

    email = array.delete_at(0)
    array.each do |item|
      duplicates << [email, item] if email_is_duplicated?(email, item)
    end
    retrieve_comparison_results(array, duplicates)
  end

  def email_is_duplicated?(email1, email2)
    # There is a posssible typo so the email is duplicated
    compare_duplicate(email1: email1, email2: email2) == 1
  end

  def compare_duplicate(email1:, email2:)
    matrix = [(0..email1.length).to_a]

    (1..email2.length).each do |i|
      matrix << [i] + [0] * email1.length
      iterate_email(i, email1, email2, matrix)
    end
    matrix.last.last
  end

  def iterate_email(i, email1, email2, matrix)
    (1..email1.length).each do |j|
      matrix[i][j] = calculate_distance(i, j, email1, email2, matrix)
    end
  end

  def calculate_distance(i, j, email1, email2, matrix)
    if email1[j - 1] == email2[i - 1]
      matrix[i - 1][j - 1]
    else
      [
        matrix[i - 1][j],
        matrix[i][j - 1],
        matrix[i - 1][j - 1]
      ].min + 1
    end
  end
end
