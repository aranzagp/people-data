# Salesloft Challenge

This guide is meant to help you get up and running on development.

## Versions
- Ruby 3.0.0
- Rails 6.1.3

## Dependencies

- Postgres
- yarn

## Environment variables

Create a `.env` file and set the specified variables in .env.example

## Bundle

```
bundle install
```

## Datbabase

Run:

```
rails db:create
```

## Start the server

```
bin/rails s
bin/webpack-dev-server
```

## Run the tests

```
rails test
```

## Before pushing changes

We have `rubocop` and `eslint` for code quality.

```
bundle exec rubocop
```
```
yarn lint
```

