# frozen_string_literal: true

require "test_helper"

class PeopleServiceTest < ActiveSupport::TestCase
  test "should retrieve_people_information" do
    body = IO.read('./test/fixtures/files/successfull_people_list.json')
    stub_request(:get, PeopleService::PEOPLE_URL)
      .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
      .to_return(
        status: 200, body: body, headers: { "Content-Type" => "application/json" }
      )

    people_information = PeopleService.retrieve_people_information

    parsed_information = [
      { "display_name" => "Frodo Baggins", "email_address" => "frodobaggins@gmail.com", "title" => "Hiker" },
      { "display_name" => "Bilbo Baggins", "email_address" => "bilbobaggins@gmail.com", "title" => "My Job" },
      { "display_name" => "Samwise Gamgee", "email_address" => "last@me.com", "title" => "Friend" }
    ]
    assert people_information == parsed_information
  end

  test "should return an error with invalid credentials" do
    body = "{\"error\": \"Invalid Bearer token\"}"
    stub_request(:get, PeopleService::PEOPLE_URL)
      .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
      .to_return(
        status: 401, body: body, headers: { "Content-Type" => "application/json" }
      )

    exception = assert_raises HttpRequestError do
      PeopleService.retrieve_people_information
    end

    assert_equal("401 Invalid Bearer token - https://api.salesloft.com/v2/people.json - {\"error\": \"Invalid Bearer token\"}",
                 exception.message)
  end

  test "should retrieve character frequency count" do
    body = IO.read('./test/fixtures/files/successfull_people_list.json')
    stub_request(:get, PeopleService::PEOPLE_URL)
      .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
      .to_return(
        status: 200, body: body, headers: { "Content-Type" => "application/json" }
      )

    frequency_count = PeopleService.retrieve_frequency_count
    assert_equal(
      [{
        "frodobaggins@gmail.com" => { "g" => 3, "o" => 3, "a" => 2, "m" => 2, "i" => 2,
                                      "d" => 1, "c" => 1, "l" => 1, "s" => 1,
                                      "n" => 1, "b" => 1, "r" => 1, "f" => 1 },
        "bilbobaggins@gmail.com" => { "b" => 3, "i" => 3, "g" => 3, "l" => 2, "a" => 2,
                                      "o" => 2, "m" => 2, "n" => 1, "c" => 1, "s" => 1 },
        "last@me.com" => { "m" => 2, "c" => 1, "e" => 1, "o" => 1, "t" => 1, "s" => 1, "a" => 1, "l" => 1 }
      }], frequency_count
    )
  end

  test "should return an error with invalid credentials when getting frequency count" do
    body = "{\"error\": \"Invalid Bearer token\"}"
    stub_request(:get, PeopleService::PEOPLE_URL)
      .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
      .to_return(
        status: 401, body: body, headers: { "Content-Type" => "application/json" }
      )

    exception = assert_raises HttpRequestError do
      PeopleService.retrieve_frequency_count
    end

    assert_equal("401 Invalid Bearer token - https://api.salesloft.com/v2/people.json - {\"error\": \"Invalid Bearer token\"}",
                 exception.message)
  end

  test "should retrieve possible duplicate emails" do
    body = IO.read('./test/fixtures/files/people_with_possible_duplicates.json')
    stub_request(:get, PeopleService::PEOPLE_URL)
      .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
      .to_return(
        status: 200, body: body, headers: { "Content-Type" => "application/json" }
      )

    retrieve_duplicates = PeopleService.retrieve_duplicates

    duplicates = [['frodobaggins@gmail.com', 'frodobagginss@gmail.com'],
                  ['bilbobaggins@gmail.com', 'bilbobagins@gmail.com']]

    assert retrieve_duplicates == duplicates
  end
end
