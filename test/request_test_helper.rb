# frozen_string_literal: true

module RequestTestHelper
  def stub_successfull_people_list
    # byebug
    # stub_request(:get, PeopleService::PEOPLE_URL).
    #   to_return(status: 200, body: File.read('./test/fixtures/jsons/successfull_people_list.json'))
    WebMock.stub_request(:get, PeopleService::PEOPLE_URL).with(
      headers: {
        # 'Accept'=>'*/*',
        # 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}",
        "Content-Type" => "application/json"
        # 'User-Agent'=>'Ruby'
      }
    ).to_return(status: 200, body: File.read('./test/fixtures/jsons/successfull_people_list.json'), headers: {})
  end
end
