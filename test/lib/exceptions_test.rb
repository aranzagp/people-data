# frozen_string_literal: true

require 'test_helper'

class ExceptionsTest < ActiveSupport::TestCase
  test 'check' do
    code = 401
    exception = HttpRequestError.new(code, "Error", { error: "Error" }, PeopleService::PEOPLE_URL)

    assert_equal exception.code, code
    assert_equal exception.message, "401 Error - #{PeopleService::PEOPLE_URL} - {:error=>\"Error\"}"
    assert_equal exception.response, { error: "Error" }
    assert_equal exception.url, PeopleService::PEOPLE_URL
  end
end
