# frozen_string_literal: true

require 'test_helper'

module Api
  class PeopleControllerTest < ActionDispatch::IntegrationTest
    test 'should retrieve the user information succesfully' do
      body = IO.read('./test/fixtures/files/successfull_people_list.json')
      stub_request(:get, PeopleService::PEOPLE_URL)
        .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
        .to_return(
          status: 200, body: body, headers: { "Content-Type" => "application/json" }
        )
      correct_response = [
        { "display_name" => "Frodo Baggins", "email_address" => "frodobaggins@gmail.com", "title" => "Hiker" },
        { "display_name" => "Bilbo Baggins", "email_address" => "bilbobaggins@gmail.com", "title" => "My Job" },
        { "display_name" => "Samwise Gamgee", "email_address" => "last@me.com", "title" => "Friend" }
      ]

      get api_people_path

      assert_equal response.body, correct_response.to_json
      assert_equal response.code, "200"
    end

    test 'should return errors' do
      body = "{\"error\": \"Invalid Bearer token\"}"
      stub_request(:get, PeopleService::PEOPLE_URL)
        .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
        .to_return(
          status: 401, body: body, headers: { "Content-Type" => "application/json" }
        )
      api_error = '{"error": "Invalid Bearer token"}'

      error_response = { "status" => "error",
                         "message" => "401 Invalid Bearer token - https://api.salesloft.com/v2/people.json - #{api_error}",
                         "code" => 401 }

      get api_people_path

      assert_equal response.body, error_response.to_json
    end
  end
end
