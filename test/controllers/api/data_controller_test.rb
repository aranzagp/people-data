# frozen_string_literal: true

require 'test_helper'

module Api
  class DataControllerTest < ActionDispatch::IntegrationTest
    test 'should retrieve the people character frequency count' do
      body = IO.read('./test/fixtures/files/successfull_people_list.json')
      stub_request(:get, PeopleService::PEOPLE_URL)
        .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
        .to_return(
          status: 200, body: body, headers: { "Content-Type" => "application/json" }
        )

      correct_response = [
        { "frodobaggins@gmail.com" => { "g" => 3, "o" => 3, "a" => 2, "m" => 2, "i" => 2,
                                        "d" => 1, "c" => 1, "l" => 1, "s" => 1, "n" => 1, "b" => 1, "r" => 1, "f" => 1 },
          "bilbobaggins@gmail.com" => { "b" => 3, "i" => 3, "g" => 3, "l" => 2, "a" => 2, "o" => 2,
                                        "m" => 2, "n" => 1, "c" => 1, "s" => 1 },
          "last@me.com" => { "m" => 2, "c" => 1, "e" => 1, "o" => 1, "t" => 1, "s" => 1, "a" => 1, "l" => 1 } }
      ]

      get api_frequency_count_path

      assert_equal response.body, correct_response.to_json
      assert_equal response.code, "200"
    end

    test 'should return errors' do
      body = "{\"error\": \"Invalid Bearer token\"}"
      stub_request(:get, PeopleService::PEOPLE_URL)
        .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
        .to_return(
          status: 401, body: body, headers: { "Content-Type" => "application/json" }
        )
      api_error = '{"error": "Invalid Bearer token"}'
      error_response = { "status" => "error",
                         "message" => "401 Invalid Bearer token - https://api.salesloft.com/v2/people.json - #{api_error}",
                         "code" => 401 }

      get api_frequency_count_path

      assert_equal response.body, error_response.to_json
    end

    test 'should retrieve the possible duplicate emails' do
      body = IO.read('./test/fixtures/files/people_with_possible_duplicates.json')
      stub_request(:get, PeopleService::PEOPLE_URL)
        .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
        .to_return(
          status: 200, body: body, headers: { "Content-Type" => "application/json" }
        )

      duplicates = [['frodobaggins@gmail.com', 'frodobagginss@gmail.com'],
                    ['bilbobaggins@gmail.com', 'bilbobagins@gmail.com']]

      get api_possible_duplicates_path

      assert_equal response.body, duplicates.to_json
      assert_equal response.code, "200"
    end

    test 'should return errors when getting possible duplicates' do
      body = "{\"error\": \"Invalid Bearer token\"}"
      stub_request(:get, PeopleService::PEOPLE_URL)
        .with(headers: { 'Authorization' => "Bearer #{ENV['SALESLOFT_API_KEY']}" })
        .to_return(
          status: 401, body: body, headers: { "Content-Type" => "application/json" }
        )
      api_error = '{"error": "Invalid Bearer token"}'
      error_response = { "status" => "error",
                         "message" => "401 Invalid Bearer token - https://api.salesloft.com/v2/people.json - #{api_error}",
                         "code" => 401 }

      get api_possible_duplicates_path

      assert_equal response.body, error_response.to_json
    end
  end
end
